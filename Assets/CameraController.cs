﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Assertions.Must;

public class CameraController : MonoBehaviour
{
    public Vector2 Tolerance;
    public Vector2 Speed;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        camera = GetComponent<Camera>();
    }

    void Update()
    {
        var player_pos = player.transform.position;
        var pos = transform.position;
        var diff = new Vector2(player_pos.x, player_pos.y) - new Vector2(pos.x, pos.y);
        var dir = Vector2.zero;
        if (Math.Abs(diff.x) > Tolerance.x)
        {
            dir.x += diff.normalized.x;
        }

        if (Math.Abs(diff.y) > Tolerance.y)
        {
            dir.y += diff.normalized.y;
        }

        transform.Translate(Speed * (dir * Time.deltaTime));
    }

    private GameObject player;
    private float pixelsToUnits = 100.0f;
    private Camera camera;
}