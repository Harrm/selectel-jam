﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using InputDevice = UnityEngine.InputSystem.InputDevice;

public class BlocksMazeInput : MonoBehaviour
{
    [NotNull] public Camera Camera;

    public float CellSize = 0.1f;

    private void Start()
    {
        mouse = Mouse.current;
        field = new Rect();
        var topLeft = new Vector2();
        var bottomRight = new Vector2();
        foreach (Transform wall in transform.Find("walls"))
        {
            topLeft.x = Math.Max(topLeft.x, wall.position.x);
            topLeft.y = Math.Max(topLeft.y, wall.position.y);
            bottomRight.x = Math.Min(bottomRight.x, wall.position.x);
            bottomRight.y = Math.Min(bottomRight.y, wall.position.y);
        }

        Debug.DrawLine(topLeft, bottomRight, Color.yellow, 5.0f);
        field.x = bottomRight.x;
        field.y = bottomRight.y;
        field.width = topLeft.x - bottomRight.x;
        field.height = topLeft.y - bottomRight.y;
    }

    private void Update()
    {
        for (int i = 0; i < field.width / CellSize; i++)
        {
            var x = field.x + CellSize / 2 + CellSize * i;
            Debug.DrawLine(new Vector3(x, field.y), new Vector3(x, field.y + field.height), Color.white);
            for (int j = 0; j < field.height / CellSize; j++)
            {
                var y = field.y + CellSize / 2 + CellSize * i;
                Debug.DrawLine(new Vector3(field.x, y), new Vector3(field.x + field.width, y), Color.white);
            }
        }
    }

    private void FixedUpdate()
    {
        if (Keyboard.current.escapeKey.isPressed)
        {
            SceneManager.LoadScene("Ice Age");
        }
        
        var mouse_pos = mouse.position.ReadValue();
        var point = Camera.ScreenToWorldPoint(new Vector3(mouse_pos.x, mouse_pos.y, 0.0f));
        point.z = 0.0f;
        var point2d = new Vector2(point.x, point.y);
        if (mouse.leftButton.wasPressedThisFrame)
        {
            var collider = Physics2D.OverlapPoint(point2d);
            if (collider && collider.CompareTag("block"))
            {
                grasped_block = collider.gameObject;
            }
        }

        if (mouse.leftButton.wasReleasedThisFrame)
        {
            if (grasped_block)
            {
                ClipToNearestCell();
            }

            grasped_block = null;
        }

        if (grasped_block == null) return;
        if (field.Contains(point))
        {
            RaycastHit2D[] hits = new RaycastHit2D[2];
            var dir = point - grasped_block.transform.position;
            var hits_num = Physics2D.BoxCastNonAlloc(grasped_block.transform.position,
                grasped_block.GetComponent<BoxCollider2D>().size * 0.07f,
                0.0f, dir.normalized, hits, dir.magnitude);
            Debug.Log(hits_num);
            if (hits_num == 0 || (hits_num == 1 && hits[0].collider.transform == grasped_block.transform))
            {
                grasped_block.transform.SetPositionAndRotation(point, Quaternion.identity);
            }
            else
            {
                Debug.Log(hits[0].collider.gameObject + " " + hits[1].collider.gameObject);
            }
        }
    }

    Vector2[] GetAdjacentCellsPos(Vector2 cell_pos)
    {
        return new[]
        {
            cell_pos + Vector2.right * CellSize,
            cell_pos + Vector2.up * CellSize,
            cell_pos + Vector2.left * CellSize,
            cell_pos + Vector2.down * CellSize
        };
    }

    void ClipToNearestCell()
    {
        var block_pos = new Vector2(grasped_block.transform.position.x, grasped_block.transform.position.y);
        var cell = GetCellPos(GetNearestCell(block_pos));
        
        var movement = cell - block_pos;
        Debug.DrawLine(grasped_block.transform.position,
            grasped_block.transform.position + new Vector3(movement.x, movement.y, 0.0f), Color.red, 1.0f);
        grasped_block.transform.Translate(movement);
    }

    Vector2Int GetNearestCell(Vector2 worldPos)
    {
        var localPos = worldPos - field.min;
        return new Vector2Int((int) Math.Round(localPos.x / CellSize), (int) Math.Round(localPos.y / CellSize));
    }

    Vector2 GetCellPos(Vector2Int cell)
    {
        return field.min + new Vector2(cell.x, cell.y) * CellSize;
    }

    private Mouse mouse;
    private GameObject grasped_block;
    private Rect field;
}