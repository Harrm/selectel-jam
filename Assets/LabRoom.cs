﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LabRoom : MonoBehaviour
{
    private static bool entered = false;
    
    // Start is called before the first frame update
    void Awake()
    {
        if (entered)
        {
            GetComponent<PlayableDirector>().enabled = false;
        }
        else
        {
            entered = true;
        }
    }
}
