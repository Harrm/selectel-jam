﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyuishBehaviour : MonoBehaviour
{
    public float ScratchPerMinute = 10.0f;
    public float SniffPerMinute = 5.0f;

    void Start()
    {
        animator = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (Random.Range(0.0f, 3600.0f) < ScratchPerMinute)
        {
            Debug.Log("Scratch");
            animator.SetTrigger("Scratch");            
        }
        if (Random.Range(0.0f, 3600.0f) < SniffPerMinute)
        {
            Debug.Log("Sniff");
            animator.SetTrigger("Sniff");            
        }
    }

    private Animator animator;
}
