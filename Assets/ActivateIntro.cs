﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ActivateIntro : MonoBehaviour
{
    public GameObject Intro;
    public GameObject Outro;
    public static bool enteredMaze = false;
    public Transform PlayerReturnSpawn;
    public CinemachineVirtualCamera PlayerCamera;
    
    // Start is called before the first frame update
    void Start()
    {
        if (enteredMaze == true)
        {
            var player = GameObject.FindWithTag("Player");
            player.transform.position = PlayerReturnSpawn.position;
            PlayerCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0.5f;
            StartCoroutine(WaitToBreakWall());
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    IEnumerator WaitToBreakWall()
    {
        yield return new WaitForSeconds(3);
        Outro.SetActive(true);

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !enteredMaze)
        {
            Intro.SetActive(true);
        }
    }

    public void StartMaze()
    {
        enteredMaze = true;
        SceneManager.LoadScene("BlocksPuzzle");
    }
}
