﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PushButton : MonoBehaviour
{
    public Sprite Off;
    public Sprite On;
    
    public void OnPressed()
    {
        GetComponent<Button>().image.sprite = On;
    }

    public void OnReleased()
    {
        GetComponent<Button>().image.sprite = Off;
    }
}
