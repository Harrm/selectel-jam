﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapButton : MonoBehaviour
{
    public LabPuzzleLogic GameLogic;
    public uint FirstIdx;
    public uint SecondIdx;
    
    public void OnClick()
    {
        GameLogic.Swap(FirstIdx, SecondIdx);
    }
}
