﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = System.Object;

namespace LabPuzzle
{

    public class Flask : MonoBehaviour
    {
        public const uint MaxLevelPossible = 12;
        public uint MaxLevel = MaxLevelPossible;
        public uint CurrentLevel = MaxLevelPossible;

        public SpriteRenderer Fill;
        public ParticleSystem Bubbles;

        public float LowLevelLifetime = 0.43f;
        public float HighLevelLifetime = 1.5f;

        public float LowLevelSpeed = 0.05f;
        public float HighLevelSpeed = 0.4f;

        public float Height = 1.0f;
        
        public bool IsTarget = false;

        public GameObject TargetLineRendererObject;
        
        void Start()
        {
            limitLine = GetComponent<LineRenderer>();
            limitLine.startColor = new Color(0.54f, 0, 0);
            limitLine.endColor = new Color(0.54f, 0, 0);
            limitLine.startWidth = 0.015f;
            limitLine.endWidth = 0.015f;

            if (IsTarget)
            {
                var lineHolder = Instantiate(TargetLineRendererObject, transform);
                targetLine = lineHolder.GetComponent<LineRenderer>();
                targetLine.startColor = Color.yellow;
                targetLine.endColor = Color.yellow;
                targetLine.startWidth = 0.015f;
                targetLine.endWidth = 0.015f;
            }
        }

        void Update()
        {
            if (CurrentLevel != current_level)
            {
                SetCurrentLevel(CurrentLevel);
            }

            if (MaxLevel != MaxLevelPossible)
            {
                var height = Height / 2.0f;
                height -= (Height / MaxLevelPossible) * (MaxLevelPossible - MaxLevel);
                limitLine.SetPosition(0, transform.position + Vector3.up * height + 0.12f * Vector3.right);
                limitLine.SetPosition(1, transform.position + Vector3.up * height + 0.12f * Vector3.left);
            }

            if (IsTarget)
            {
                targetLine.SetPosition(0, transform.position + 0.12f * Vector3.right);
                targetLine.SetPosition(1, transform.position + 0.12f * Vector3.left);
            }
        }

        void SetCurrentLevel(uint level)
        {
            level = Math.Min(level, MaxLevel);
            current_level = level;
            var k = 1 - (float) level / MaxLevelPossible;
            Fill.transform.localPosition = new Vector3(0, -5.86f * k, 0);
            var main = Bubbles.main;
            if (current_level != 0)
            {
                main.startLifetime = LowLevelLifetime + (HighLevelLifetime - LowLevelLifetime) * (1 - k);
                main.startSpeed = LowLevelSpeed + (HighLevelSpeed - LowLevelSpeed) * (1 - k);
            }
            else
            {
                main.startLifetime = 0;
                main.startSpeed = 0;
            }
        }

        private uint current_level = MaxLevelPossible;
        private LineRenderer limitLine = null;
        private LineRenderer targetLine = null;
    }
}