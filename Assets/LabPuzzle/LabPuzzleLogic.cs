﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using LabPuzzle;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LabPuzzleLogic : MonoBehaviour
{
    public Flask[] Flasks = new Flask[3];

    public void Transfer()
    {
        var amount = Math.Min(Flasks[0].MaxLevel - Flasks[0].CurrentLevel, Flasks[1].CurrentLevel);
        Debug.Log("Transfer " + amount);
        Flasks[0].CurrentLevel += amount;
        Flasks[1].CurrentLevel -= amount;

        if (Flasks[0].CurrentLevel == 6 || Flasks[1].CurrentLevel == 6)
        {
            SceneManager.LoadScene("Laboratory");
        }
    }

    public void Swap(uint first, uint second)
    {
        Debug.Log("Swap " + first + " and " + second);
        {
            var temp = Flasks[first].transform.position;
            Flasks[first].transform.position = Flasks[second].transform.position;
            Flasks[second].transform.position = temp;
        }
        {
            var temp = Flasks[first];
            Flasks[first] = Flasks[second];
            Flasks[second] = temp;
        }
    }
}