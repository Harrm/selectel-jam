﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

public class PipesSpinController_noTiles : MonoBehaviour
{
    private Mouse mouse;
    private GameObject[] pipesArray;
    private bool isPipesPathRight = true;

    void Start()
    {
        mouse = Mouse.current;
        foreach (Transform pipe in transform)
        {
            int randNum = Random.Range(1, 4);
            pipe.Rotate(0.0f, 0.0f, 90.0f * randNum, Space.World);
        }
    }


    void Update()
    {
        foreach (Transform pipe in transform)
        {
            var pipeRotation = (int) System.Math.Abs(pipe.rotation.eulerAngles.z);
            if (pipe.CompareTag("Curved pipe"))
            {
                if (pipeRotation % 360 != 0)
                {
                    isPipesPathRight = false;
                }
            }
            else if (pipe.CompareTag("Straight pipe"))
            {
                if (pipeRotation % 180 != 0)
                {
                    isPipesPathRight = false;
                }
            }
        }

        //Debug.Log(isPipesPathRight);
        if (isPipesPathRight)
        {
            transform.parent.gameObject.SetActive(false); //gameObject.SetActive(false);
        }
        else
        {
            isPipesPathRight = true;
        }
    }

    void FixedUpdate()
    {
        if (mouse == null)
        {
            return;
        }
        var mouse_pos = mouse.position.ReadValue();
        var point = Camera.main.ScreenToWorldPoint(new Vector3(mouse_pos.x, mouse_pos.y, 0.0f));
        point.z = 0.0f;
        var point2d = new Vector2(point.x, point.y);
        if (mouse.leftButton.wasPressedThisFrame)
        {
            var collider = Physics2D.OverlapPoint(point2d);
            if (collider && (collider.CompareTag("Curved pipe") || collider.CompareTag("Straight pipe")))
            {
                collider.transform.Rotate(0.0f, 0.0f, 90.0f, Space.World);
            }
        }
    }
}