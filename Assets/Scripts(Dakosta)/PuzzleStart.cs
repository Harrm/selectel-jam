﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PuzzleStart : MonoBehaviour
{
    private Mouse mouse;
    [SerializeField]
    private GameObject puzzle;

    void Start()
    {
        mouse = Mouse.current;
    }
    void FixedUpdate() 
    {
        var mouse_pos = mouse.position.ReadValue();
        var point = Camera.main.ScreenToWorldPoint(new Vector3(mouse_pos.x, mouse_pos.y, 0.0f));
        point.z = 0.0f;
        var point2d = new Vector2(point.x, point.y);
        var position2d = new Vector2(transform.position.x, transform.position.y);
        if (mouse.leftButton.wasPressedThisFrame)
        {
            var collider = Physics2D.OverlapPoint(point2d);
            var myCollider = Physics2D.OverlapPoint(position2d);
            if (collider == myCollider)
            {
                
                puzzle.SetActive(true);
            }
        }        
    }
}