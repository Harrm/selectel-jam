﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterMovement : MonoBehaviour
{
    public float Speed = 5.0f;
    public float MaxSpeed = 10.0f;

    private void Start()
    {
        controls = GetComponent<PlayerInput>();
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        var v = controls.actions["Walk"].ReadValue<Vector2>();
        rigidbody.AddForce(Speed * v, ForceMode2D.Force);
        var scale = transform.localScale;
        var velocity_x = rigidbody.velocity.x;
        var shouldFlipRight = velocity_x < -0.01f && scale.x > 0.0f;
        var shouldFlipLeft = velocity_x > 0.01f && scale.x < 0.0f;
        if (shouldFlipRight || shouldFlipLeft)
        {
            transform.localScale = new Vector3(-1.0f * scale.x, scale.y, scale.z);
        }
        rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, MaxSpeed);
        animator.SetFloat("Speed", rigidbody.velocity.magnitude);
    }

    private new Rigidbody2D rigidbody;
    private PlayerInput controls;
    private Animator animator;
}