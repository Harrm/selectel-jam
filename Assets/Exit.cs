﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    public Animator FadePanel;
    public string NextScene;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            FadePanel.SetTrigger(FadeOutProperty);
            StartCoroutine(WaitForSceneLoad());
        }
    }
    
    private IEnumerator WaitForSceneLoad()
    {
        yield return new WaitForSeconds(0.8f);
        SceneManager.LoadScene(NextScene);
    }
    
    
    
    private static readonly int FadeOutProperty = Animator.StringToHash("Fade Out");
}
