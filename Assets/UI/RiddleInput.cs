﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class RiddleInput : MonoBehaviour
{
    public string RiddleText = "Riddle";
    public List<string> Choices = new List<string>();

    private void Start()
    {
        var buttons = GetComponentsInChildren<Button>();
        Assert.AreEqual(Choices.Count, buttons.Length);
        var i = 0;
        foreach (var button in buttons)
        {
            var text = button.GetComponentInChildren<Text>();
            text.text = Choices[i];
            i++;
        }
    }
}