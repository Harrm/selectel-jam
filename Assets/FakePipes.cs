﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakePipes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform pipe in transform){
            int randNum = Random.Range(1, 4);
            pipe.Rotate(0.0f, 0.0f, 90.0f * randNum, Space.World);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
